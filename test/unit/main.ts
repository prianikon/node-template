import { describe, it } from 'mocha';
import assert from 'assert';

describe('Группа тестов', () => {
  describe('Подгруппа тестов', () => {
    it('Успешный тест', async () => {
      assert.strictEqual(
        '1',
        '1',
      );
    });
    it('Проваленный тест', async () => {
      assert.strictEqual(
        'получено',
        'ожидалось',
      );
    });
  });
});
