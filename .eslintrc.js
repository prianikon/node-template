module.exports = {
  'extends': [
    'airbnb-base',
  ],
  'plugins': [
    '@typescript-eslint',
  ],
  'parser': '@typescript-eslint/parser',
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
};
